# Downloadstand (legacy ESP-IDF)

Residents of the Studierendenwerk Hamburg can access the amount of their used data volume by sending a mail to the downloadstand service of the Studierendenwerk Hamburg. This program for the ESP32 automates this process by sending a mail every 30 minutes, extracting the downloaded amount of data from the response and outputting it on its log, which can be viewed via a serial connection and [PuTTY](https://www.putty.org/) or the built-in `make monitor` tool.

## Installation

This project uses the old ESP-IDF [v3.1](https://github.com/espressif/esp-idf/releases/tag/v3.1) and the "make" toolchain.

First, rename "Mail_dummy.h" and "WLAN_dummy.h" to "Mail.h" and "WLAN.h" and provide the details for your Wifi and mail account. After this, make sure the custom partions table "partitions_example.csv" is used with the standard offset and flash the project to your ESP.

### SPIFFS partition

To create a SPIFFS partition with the right certificates for your server, provide a (root) certificate for your server as "cacert.pem" in the folder "spiffs". Then make sure to update and build the submodule `mkspiffs`:
```bash
git submodule update --init --recursive
cd mkspiffs
make dist
```
After this, you can run "make_spiffs.sh" to create the image based on the folder spiffs. It will be generated in the root folder under the name "spiffs.bin".
Then run "flash_stuff.sh" to flash the SPIFFS partition with the certificate to your ESP.

Now you are done! You can monitor your ESP using the method of your choice.
