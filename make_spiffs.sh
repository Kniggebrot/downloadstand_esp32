#!/usr/bin/bash
if [ "$OSTYPE" == "msys" ]; then
    mkspiffs/mkspiffs.exe -c spiffs -b 4096 -p 256 -s 303104 spiffs.bin
else
    mkspiffs/mkspiffs -c spiffs -b 4096 -p 256 -s 303104 spiffs.bin
fi

# Sure, could check for more systems, but...